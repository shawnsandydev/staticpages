<?php

namespace ShawnSandy\StaticPages;

use Illuminate\Support\ServiceProvider;

class StaticPagesServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Register package routes
         */
        include __DIR__ .'/routes.php';

        /*
         * Register page views
         */
        $this->loadViewsFrom(__DIR__.'/views', 'static-pages');

    }
}
