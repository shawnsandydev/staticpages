<?php
/**
 * Create static pages for your laravel site
 * @author Shawn Sandy
 * @copyright Shawn Sandy
 * @link http://
 * @package Static Pages
 */


Route::get('page/', function(){
    return view('static-pages::home');
});


Route::get('page/{name}', function($name){
    return view('static-pages::'.$name);
});
